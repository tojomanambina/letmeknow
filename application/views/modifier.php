
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table.css'); ?>">
    <title></title>
</head>
<body>
    <form action="<?php echo site_url('Accueil/Insert?page=Liste'); ?>" method="post">

        <input type="hidden" name="id" value="<?php echo $notes[$index]['numero_etudiant'];?>">
        <label for="nom">Nom Etudiant</label>
        <input type="text" value="<?php echo $notes[$index]['nom_etudiant'];?>" name="nom" id="">

        <label for="nom">date de naissance</label>
        <input type="date" name="date_naissance" id="date_naissance" value="<?php echo $notes[$index]['date_naissance'];?>">        

        <label for="notes">Notes</label>
        <input type="number" value="<?php echo $notes[$index]['note'];?>" name="notes" id="" step="0.001">

        <label for="matiere">Matiere</label>
        <select name="matiere" id="">
        <option value="<?php echo $notes[$index]['code_matiere'];?>"><?php echo $notes[$index]['nom_matiere'];?></option>


        <?php
            for ($i=0; $i < count($matiere); $i++) { ?>
            <option value="<?php echo $matiere[$i]['code_matiere'];?>">
                <?php echo $matiere[$i]['nom_matiere'];?>
            </option>
        <?php } ?>
        </select>

        <input type="submit" value="Valider">

    </form>
</body>
</html>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table.css'); ?>">
    <title></title>
</head>
<body>
<h1> Note par Matiere </h1>
    <form action="<?php echo site_url('Accueil/insertEtudiant?page=Liste'); ?>" method="post">
        <label for="nomEtudiant">Nom</label>
        <input type="text" name="nomEtudiant" required>
        
        <label for="note">Date de naissance</label>
        <input type="date" name="dateNaissance" required>
        
        <label for="nom_matiere">Matiere:</label>
        <select name="matiere" id="" required>
            <?php for ($i=0; $i < count($matiere); $i++)  { ?>
                <option value="<?php echo $matiere[$i]['code_matiere']; ?>"><?php echo $matiere[$i]['nom_matiere']; ?></option>
                <?php } ?>
            </select>
            
            <input type="submit" value="Ajouter">
        </form>
        <label for="note">Note:</label>
        <input type="number" name="note" required>
        
<h1> Note par Matiere </h1>
<table class="container">
    <thead>
        <tr>
            <th>Nom Etudiant</th>
            <th>Note</th>
            <th>Matiere</th>
        </tr>
    </thead>
    <tbody>
        <?php
            for ($i=0; $i < count($notes); $i++) 
            { 
                ?>
            <tr>
                <td><?php echo $notes[$i]['nom_etudiant'];?></td>
                <td><?php echo $notes[$i]['note'];?></td>
                <td><?php echo $notes[$i]['nom_matiere']; ?></td>
                <td><a href="<?php echo site_url('Accueil/modif?id='.$i.'&page=modifier'); ?>" >Modifier</a></td>
            </tr>
            <?php } ?>
    </tbody>
    </table>
    </body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Horoscope</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/model.css">
</head>
<body>
    <h1>Créer une Publication</h1>
    <?php echo validation_errors(); ?>
    <?php if (isset($error)) { echo $error; } ?>
    <?php if (isset($success)) { echo $success; } ?>
    <?php echo form_open_multipart('User/create_publication'); ?>
    <form action="<?php echo site_url('User/create_publication'); ?>" method="post">
    <label for="texte">Texte:</label>
    <input type="text" name="texte" required/><br/>
    <label for="image">Image:</label>
    <input type="file" name="image" /><br />

    <input type="submit" value="Publier" />
    </form>

    <a href="<?php echo site_url('User/TousLesPublication'); ?>">Afficher toutes les publications</a>
    <div class="containerPublication">
        <div class="cartePub">
        <?php for ($i = 0; $i < count($pub); $i++) { ?>
            <div>
                <p>Texte: <?php echo $pub[$i]->texte; ?></p>
                <img src="<?php echo base_url($pub[$i]->imagesUrl . '.png'); ?>" alt="Publication Image">
                <p>Publié par: <?php echo $pub[$i]->pseudo; ?></p>
                <p>Date de publication: <?php echo $pub[$i]->datePub; ?></p>
            </div>
        <?php } ?>
        </div>
    </div>
</body>
</html>

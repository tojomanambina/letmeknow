<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table.css'); ?>">
    <script  src="<?php echo base_url('assets/js/script.js'); ?>"></script>
    <title>Document</title>
</head>
<body>
    <h1> Moyenne </h1>
<table class="container">
    <thead>
        <tr>
            <th>Rang</th>
            <th>Nom Etudiant</th>
            <th>Moyenne</th>
        </tr>
    </thead>
    <tbody>
        <?php
            for ($i=0; $i < count($moyenne); $i++) 
            { 
                ?>
            <tr>
                <td><?php echo $i + 1;?></td>
                <td><?php echo $moyenne[$i]['nom_etudiant'];?></td>
                <td><?php echo number_format($moyenne[$i]['Moyenne'], 2); ?></td>
            </tr>
            <?php } ?>
    </tbody>
    </table>
</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accueil extends CI_Controller 
{
	public function __construct() {
		parent::__construct();
		$this->load->model('Mydata');
	}
	public function index2()
	{
		$this->load->view('index');
	}
	public function info()
	{
		$data = array();
		$data['lesListes']  = $this->input->get('page');
		$data['info'] = $this->Mydata->getEtudiantInfo();
        $this->load->view('Acceuil', $data);
	}
	public function liste()
	{
		$data['lesListes']  = $this->input->get('page');
		$data['matiere']  = $this->Mydata->Matiere();
		$data['notes'] = $this->Mydata->Note();
		$this->load->view('Acceuil', $data);
	}

	public function moyenne()
	{
		$data['lesListes']  = $this->input->get('page');
		$data['moyenne'] = $this->Mydata->Moyenne();
		$this->load->view('Acceuil', $data);
	}

	public function modif()
	{
		$data['lesListes']  = $this->input->get('page');
		$data['index']  = $this->input->get('id');
		$data['matiere']  = $this->Mydata->Matiere();
		$data['notes']  = $this->Mydata->Note();
		$this->load->view('Acceuil', $data);
	}

	public function insertNote() 
	{
		$nom_etudiant = $this->input->post('nom_etudiant');
		$note = $this->input->post('note');
		$code_matiere = $this->input->post('code_matiere');
		$this->Mydata->insert_note($nom_etudiant, $note, $code_matiere);
		redirect('Accueil/liste?page=Liste');
	}	
	
    public function insert() 
	{
        $id = $this->input->post('id');
        $nom_etudiant = $this->input->post('nom');
        $date_naissance = $this->input->post('date_naissance');
        $notes = $this->input->post('notes');
        $matiere = $this->input->post('matiere');
        
        $update = $this->Mydata->update_all($id, $nom_etudiant, $date_naissance, $notes, $matiere);
        
        if ($update) 
		{
            redirect('Accueil/liste?page=Liste');
        } else {
            $data['error'] = "Erreur lors de l'update";
            $this->load->view('Acceuil', $data);
        }
    }

	public function insertEtudiant() 
	{
        $nom_etudiant = $this->input->post('nomEtudiant');
        $date_naissance = $this->input->post('dateNaissance');
        $matiere = $this->input->post('matiere');
        
        $update = $this->Mydata->update_all($id, $nom_etudiant, $date_naissance, $notes, $matiere);
        
        if ($update) 
		{
            redirect('Accueil/liste?page=Liste');
        } else {
            $data['error'] = "Erreur lors de l'update";
            $this->load->view('Acceuil', $data);
        }
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('Acceuil');
	}

	public function AfficheHoroscope()
	{
		$hor=array();
		$hor['Name'] = 'Belier';
		$hor['texte'] = 'Mangirana ny andronao';
		
		$hor['Name'] = 'Sagitaire';
		$hor['texte'] = 'Aza kamo zany';

		$hor['Name'] = 'Capricorne';
		$hor['texte'] = 'maizina rahalina fa aza taitra';

		$this->load->view('Horoscope',$hor);
	}		
	public function AfficheHoroscopeChinois()
	{
		$horC=array();
		$horC['Name'] = 'Rat';
		$horC['texte'] = 'mety Tsala ny Anjo Anio';

		$hor['Name'] = 'Buffle';
		$horC['texte'] = 'Mitonja elo lehefa mivoka ny Tsano';
		
		$hor['Name'] = 'Tigre';
		$horC['texte'] = 'Mila manao legimina ianao anjoany';

		$this->load->view('HoroscopeChinois',$horC);
	}		
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liste extends CI_Controller {
	


    public function __construct()
    {
        parent::__construct();
        $this->load->library('request');
    }

    public function list_url()
    {
        $list_type = $this->input->get('list');
        $view = 'model';
        $data['page'] = 'liste_'.$list_type;
        $query = "SELECT * FROM ".$list_type;
        $data[$list_type] = $this->request->executeQuery($query);

        $this->load->view($view,$data);
    }

}

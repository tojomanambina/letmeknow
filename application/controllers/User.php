<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{

    public function __construct() 
    {
		parent::__construct();
		$this->load->model('Mydata');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function index() 
    {
        $this->load->view('Acceuil');
    }

    public function login() {
        $pseudo = $this->input->post('pseudo');
        $user = $this->Mydata->get_user_by_pseudo($pseudo);

        if ($user) 
        {
            $this->session->set_userdata('user_id', $user->idUser);
            $this->load->view('pagePublication');
        } else {
            $this->load->view('Acceuil', array('error' => 'Pseudo non trouvé'));
        }
    }

    public function create_publication() 
    {
        if (!$this->session->userdata('user_id')) 
        {
            redirect('user');
        }

        $this->form_validation->set_rules('texte', 'Texte', 'required');

        if ($this->form_validation->run() == FALSE) 
        {
            $this->load->view('pagePublication');
        } else {
            $texte = $this->input->post('texte');
            $config['upload_path'] = './upload/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) 
            {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('pagePublication', $error);
            } else {
                $data = $this->upload->data();
                $image_url = 'uploads/' . $data['file_name'];
                $this->Mydata->insertPublication($texte, $image_url, $this->session->userdata('user_id'));
                $this->load->view('pagePublication', array('success' => 'Publication créée avec succès'));
            }
        }
    }
 
    public function TousLesPublication() 
    {
        $data['pub'] = $this->Mydata->getPublications();
        $this->load->view('pagePublication', $data);
    }
    
    
}
?>

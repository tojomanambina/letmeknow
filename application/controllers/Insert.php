<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insert extends CI_Controller {
	


    public function __construct()
    {
        parent::__construct();
        $this->load->library('request');
    }

    public function insert_table()
    {
        $table_type = $this->input->get('table_type');
        
        switch ($table_type) {
            case 'user':
                $pseudo = $this->input->get('pseudo');
                $value = "(pseudo) VALUES (%s)";
                $value = sprintf($value, $pseudo);

                break;
            case 'Publication':
                $texte = $this->input->get('texte');
                $imagesUrl = $this->input->get('imagesUrl');
                $datePub = $this->input->get('datePub');
                $idUser = $this->input->get('idUser');
                $value = "(texte, imagesUrl, datePub, idUser) VALUES (%s, %s, %s, %d)";
                $value = sprintf($value, $texte, $imagesUrl, $date, $idUser);

                break;
            case 'Commentaire':
                $pseudo = $this->input->get('pseudo');
                $value = "(idUser, dateComs, idPub, texte) VALUES (%s, %s, %d, %s)";
                $value = sprintf($value, $idUser, $dateComs, $idPub, $texte);
                
                break;
            default:
                echo "Action non valide";
                break;
        }
    }
    $query = "INSERT INTO %s %s";
    $query = sprintf($query, $table_type, $value);
    $data[$list_type] = $this->request->executeQuery($query);
}

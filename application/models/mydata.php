<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mydata extends CI_Model{
    public function get_user_by_pseudo($pseudo) 
    {
        $query = $this->db->get_where('user', array('pseudo' => $pseudo));
        return $query->row();
    }

    public function insertPublication($texte, $image_url, $user_id) 
    {
        $data = array
        (
            'texte' => $texte,
            'imagesUrl' => $image_url,
            'idUser' => $user_id
        );
        $this->db->insert('Publication', $data);
    }

    public function getPublications() 
    {
        $this->db->select('Publication.idPub, Publication.texte, Publication.imagesUrl, Publication.datePub, user.pseudo');
        $this->db->from('Publication');
        $this->db->join('user', 'Publication.idUser = user.idUser');
        $this->db->order_by('Publication.datePub', 'ASC');
        
        $query = $this->db->get();

        return $query->result();
    }
} 
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function executeQuery($query)
    {
        $result = $this->CI->db->query($query);
        return $result->result_array();
    }

}
?>
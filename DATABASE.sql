CREATE DATABASE letmeknow;
use letmeknow;
CREATE TABLE user 
(
    idUser INT PRIMARY KEY AUTO_INCREMENT,
    pseudo VARCHAR(255) not null
);

INSERT INTO user (pseudo) VALUES ('Andry');
INSERT INTO user (pseudo) VALUES ('Miora');
INSERT INTO user (pseudo) VALUES ('Zo');
INSERT INTO user (pseudo) VALUES ('Feno');
INSERT INTO user (pseudo) VALUES ('Tahina');
INSERT INTO user (pseudo) VALUES ('Fara');
INSERT INTO user (pseudo) VALUES ('Hery');
INSERT INTO user (pseudo) VALUES ('Lova');
INSERT INTO user (pseudo) VALUES ('Tiana');
INSERT INTO user (pseudo) VALUES ('Niry');


CREATE TABLE Publication 
(
    idPub INT PRIMARY KEY AUTO_INCREMENT,
    texte VARCHAR(255) not null,
    imagesUrl VARCHAR(30),
    datePub timestamp default CURRENT_TIMESTAMP,
    idUser int,
    FOREIGN KEY (idUser) REFERENCES user(idUser)
);

CREATE TABLE Commentaire
(
    idComs INT PRIMARY KEY AUTO_INCREMENT,
    idUser int,
    dateComs timestamp default CURRENT_TIMESTAMP,
    idPub int,
    texte VARCHAR(20),
    FOREIGN KEY (idPub) REFERENCES Publication(idPub),
    FOREIGN KEY (idUser) REFERENCES user(idUser)
);


kc4VfoiGF870
7uG9vBlasyHS